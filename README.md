## DesignLayer integrated in one docker image

### How to use

Put [DeisgnLayer](https://github.com/bigcatml/DesignLayer)(you may just need down and unzip zip archive) and the `devset` and `devset_2` sample training data into the same folder. Then build the image:

```bash
$ docker build -t designlayer ./
```

Run it with ports `8000` and `6006` exposed:

```bash
$ docker run -d -v /YOUR_MODEL_DIR/devset:/root/devset -v /YOUR_MODEL_DIR/devset_2:/root/devset_2 -it -e TRAINING_DATA_DIR=/root/devset -e TENSORBOARD_PORT=6006 -p HOST_PORT:9000 -p 6006:6006 designlayer:latest
# Or use the training data inside container
$ docker run -it -p HOST_PORT:9000 -p 6006:6006 -e TENSORBOARD_PORT=6006 -e TRAINING_DATA_DIR=/root/devset designlayer:latest
```

Access the website with `http://localhost:HOST_PORT/`
