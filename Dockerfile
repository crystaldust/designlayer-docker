FROM tensorflow/tensorflow:latest-py3
RUN apt-get update
RUN apt-get install -y nginx
RUN apt-get install -y tk python3-tk graphviz
RUN pip install Flask keras pydot tensorflow

ADD ./DesignLayer /root/DesignLayer

ADD ./designlayer-ngx /etc/nginx/sites-available/designlayer
RUN ln -s /etc/nginx/sites-available/designlayer /etc/nginx/sites-enabled/designlayer
RUN chown www-data:www-data -R /root/DesignLayer/web-server

ADD ./start.sh /root/
RUN chmod +x /root/start.sh
RUN sed -i 's/user\ www-data/user\ root/' /etc/nginx/nginx.conf

ADD ./devset /root/devset
ADD ./devset_2 /root/devset_2

CMD ["/root/start.sh"]
