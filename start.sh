#!/bin/bash
export LC_ALL=C.UTF-8
export LANG=C.UTF-8

# Enable nginx statis serving for web front
nginx # call nginx directly in a container to start the nginx server

# Start tensorboard
if [ "$TENSORBOARD_PORT" == "" ]; then
	TENSORBOARD_PORT=6006
fi
cd /root/DesignLayer/tiger
nohup tensorboard --logdir=/root/DesignLayer/micro-services/logs/ --host=0.0.0.0 --port=${TENSORBOARD_PORT} > tensorboard.log &

# Start micro service
cd /root/DesignLayer/micro-services
FLASK_APP=main.py flask run
